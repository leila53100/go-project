package config

import (
	"database/sql"
	"log"

	_ "github.com/lib/pq"
)

var db *sql.DB

//DatabaseInit : initialisation de la bdd
func DatabaseInit() {
	var err error

	db, err = sql.Open("postgres", "user=postgres password=2202041998 dbname=avion sslmode=disable")

	if err != nil {
		log.Fatal(err)
	}
	// Create Table cars if not exists
	createAvionsTable()
}

func createAvionsTable() {
	_, err := db.Exec("CREATE TABLE IF NOT EXISTS avion(id serial,materiel varchar(20), design varchar(20), style varchar(20), portes int, constraint pk primary key(id))")

	if err != nil {
		log.Fatal(err)
	}
}

// Db : Getter for db var
func Db() *sql.DB {
	return db
}
