module tp

go 1.15

require (
	github.com/Masterminds/semver/v3 v3.1.0 // indirect
	github.com/PuerkitoBio/goquery v1.5.1 // indirect
	github.com/cockroachdb/cockroach-go v2.0.1+incompatible // indirect
	github.com/fsnotify/fsnotify v1.4.9 // indirect
	github.com/gobuffalo/buffalo v0.16.15 // indirect
	github.com/gobuffalo/buffalo-pop/v2 v2.2.0 // indirect
	github.com/gobuffalo/fizz v1.13.0 // indirect
	github.com/gobuffalo/here v0.6.2 // indirect
	github.com/gobuffalo/nulls v0.4.0 // indirect
	github.com/gobuffalo/validate/v3 v3.3.0 // indirect
	github.com/gofrs/uuid v3.3.0+incompatible // indirect
	github.com/gorilla/handlers v1.5.0 // indirect
	github.com/gorilla/mux v1.8.0
	github.com/gorilla/sessions v1.2.1 // indirect
	github.com/hashicorp/hcl v1.0.0
	github.com/jackc/pgproto3/v2 v2.0.4 // indirect
	github.com/jackc/pgx/v4 v4.8.1 // indirect
	github.com/karrick/godirwalk v1.16.1 // indirect
	github.com/lib/pq v1.8.0
	github.com/magiconair/properties v1.8.2 // indirect
	github.com/mattn/go-colorable v0.1.7 // indirect
	github.com/microcosm-cc/bluemonday v1.0.4 // indirect
	github.com/mitchellh/mapstructure v1.3.3 // indirect
	github.com/pelletier/go-toml v1.8.0 // indirect
	github.com/rogpeppe/go-internal v1.6.2 // indirect
	github.com/sirupsen/logrus v1.6.0 // indirect
	github.com/spf13/afero v1.3.5 // indirect
	github.com/spf13/cast v1.3.1 // indirect
	github.com/spf13/cobra v1.0.0 // indirect
	github.com/spf13/jwalterweatherman v1.1.0 // indirect
	github.com/spf13/viper v1.7.1 // indirect
	github.com/synbioz/go_api v0.0.0-20150717075507-4fb3d9247c55
	golang.org/x/crypto v0.0.0-20200820211705-5c72a883971a // indirect
	golang.org/x/net v0.0.0-20200904194848-62affa334b73 // indirect
	golang.org/x/sys v0.0.0-20200905004654-be1d3432aa8f // indirect
	golang.org/x/tools v0.0.0-20200904185747-39188db58858 // indirect
	gopkg.in/ini.v1 v1.61.0 // indirect
)
