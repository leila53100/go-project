package controllers

import (
	"encoding/json"
	"io/ioutil"
	"log"
	"net/http"
	"tp/models"

	"strconv"

	"github.com/gorilla/mux"
)

//AvionsIndex : http conf
func AvionsIndex(w http.ResponseWriter, r *http.Request) {
	w.Header().Set("Content-type", "application/json;charset=UTF-8")
	w.WriteHeader(http.StatusOK)

	json.NewEncoder(w).Encode(models.AllAvions())
}

// AvionsCreate : create
func AvionsCreate(w http.ResponseWriter, r *http.Request) {
	w.Header().Set("Content-type", "application/json;charset=UTF-8")
	w.WriteHeader(http.StatusOK)

	body, err := ioutil.ReadAll(r.Body)

	if err != nil {
		log.Fatal(err)
	}

	var avion models.Avion

	err = json.Unmarshal(body, &avion)

	if err != nil {
		log.Fatal(err)
	}

	models.NewAvion(&avion)

	json.NewEncoder(w).Encode(avion)
}

//AvionShow : ze
func AvionShow(w http.ResponseWriter, r *http.Request) {
	w.Header().Set("Content-type", "application/json;charset=UTF-8")
	w.WriteHeader(http.StatusOK)

	vars := mux.Vars(r)
	id, err := strconv.Atoi(vars["id"])

	if err != nil {
		log.Fatal(err)
	}

	avion := models.FindAByID(id)

	json.NewEncoder(w).Encode(avion)
}

//AvionUpdate : màj
func AvionUpdate(w http.ResponseWriter, r *http.Request) {
	w.Header().Set("Content-type", "application/json;charset=UTF-8")
	w.WriteHeader(http.StatusOK)

	vars := mux.Vars(r)
	id, err := strconv.Atoi(vars["id"])

	if err != nil {
		log.Fatal(err)
	}

	body, err := ioutil.ReadAll(r.Body)

	if err != nil {
		log.Fatal(err)
	}

	avion := models.FindAByID(id)

	err = json.Unmarshal(body, &avion)

	models.UpdateAvions(avion)

	json.NewEncoder(w).Encode(avion)
}

//AvionsDelete : suppression avion
func AvionsDelete(w http.ResponseWriter, r *http.Request) {
	w.Header().Set("Content-type", "application/json;charset=UTF-8")
	w.WriteHeader(http.StatusOK)

	vars := mux.Vars(r)

	// strconv.Atoi is shorthand for ParseInt
	id, err := strconv.Atoi(vars["id"])

	if err != nil {
		log.Fatal(err)
	}

	err = models.DeleteAvionByID(id)
}
