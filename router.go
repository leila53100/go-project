package main

import (
	"tp/controllers"

	"github.com/gorilla/mux"
)

//InitializeRouter : déclaration des differentes routes
func InitializeRouter() *mux.Router {
	// StrictSlash is true => redirect /cars/ to /cars
	router := mux.NewRouter().StrictSlash(true)

	router.Methods("GET").Path("/avions").Name("Index").HandlerFunc(controllers.AvionsIndex)
	router.Methods("POST").Path("/avions").Name("Create").HandlerFunc(controllers.AvionsCreate)
	router.Methods("GET").Path("/avions/{id}").Name("Show").HandlerFunc(controllers.AvionShow)
	router.Methods("PUT").Path("/avions/{id}").Name("Update").HandlerFunc(controllers.AvionUpdate)
	router.Methods("DELETE").Path("/avions/{id}").Name("DELETE").HandlerFunc(controllers.AvionsDelete)
	return router
}
