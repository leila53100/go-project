package models

import (
	"log"
	"tp/config"
	
)

//Avion : Strucure de la table
type Avion struct {
	ID       int    `json:"id"`
	Materiel string `json:"materiel"`
	Design   string `json:"design"`
	Style    string `json:"style"`
	Portes   uint8  `json:"portes"`
}

//Avions : tableau
type Avions []Avion

// NewAvion : persiste une nouvelle entrée en bdd
func NewAvion(c *Avion) {
	if c == nil {
		log.Fatal(c)
	}

	err := config.Db().QueryRow("INSERT INTO avion (materiel, design, style, portes) VALUES ($1,$2,$3,$4) RETURNING id;", c.Materiel, c.Design, c.Style, c.Portes).Scan(&c.ID)

	if err != nil {
		log.Fatal(err)
	}
}

//FindAByID : permet de récupérer un enregistrement par son id
func FindAByID(ID int) *Avion {
	var avion Avion

	row := config.Db().QueryRow("SELECT * FROM avion WHERE id = $1;", ID)
	err := row.Scan(&avion.ID, &avion.Materiel, &avion.Design, &avion.Style, &avion.Portes)

	if err != nil {
		log.Fatal(err)
	}

	return &avion
}

//AllAvions : récupère tous les enregistrements de la table avions
func AllAvions() *Avions {
	var avions Avions

	rows, err := config.Db().Query("SELECT * FROM avion")

	if err != nil {
		log.Fatal(err)
	}

	// Close rows after all readed
	defer rows.Close()

	for rows.Next() {
		var a Avion

		err := rows.Scan(&a.ID, &a.Materiel, &a.Design, &a.Style, &a.Portes)

		if err != nil {
			log.Fatal(err)
		}

		avions = append(avions, a)
	}

	return &avions
}

// UpdateAvions : Mise à jour
func UpdateAvions(avion *Avion) {

	stmt, err := config.Db().Prepare("UPDATE avion SET materiel=$1, design=$2, style=$3, portes=$4 WHERE id=$6;")

	if err != nil {
		log.Fatal(err)
	}

	_, err = stmt.Exec(avion.Materiel, avion.Design, avion.Style, avion.Portes, avion.ID)

	if err != nil {
		log.Fatal(err)
	}
}

//DeleteAvionByID : suppression
func DeleteAvionByID(id int) error {
	stmt, err := config.Db().Prepare("DELETE FROM avion WHERE id=$1;")

	if err != nil {
		log.Fatal(err)
	}

	_, err = stmt.Exec(id)

	return err
}
