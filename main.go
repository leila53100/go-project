package main

import (
	"log"
	"net/http"
	"tp/config"
	"tp/models"
)

//InitializeRouter : initialiser le routeur
func main() {
	config.DatabaseInit()
	router := InitializeRouter()
	// Populate database
	models.NewAvion(&models.Avion{Materiel: "AIRFRANCE", Design: "A380", Style: "JET", Portes: 4})

	log.Fatal(http.ListenAndServe(":8080", router))
}
